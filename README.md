# wikidataEntitySummarizer

This projects aims to summarize the (sometimes redundant) classes a [wikidata](https://www.wikidata.org) entry belongs to.



A wikidata item can be directly associated to (0?), 1 or more classes. 
This can be either:
- from the `instance of` property ([P31](https://www.wikidata.org/wiki/Property:P31)) when the item refers to an individual: Marie Curie ([Q7186](https://www.wikidata.org/wiki/Q7186)) is an instance of [human](https://www.wikidata.org/wiki/Q5)
- from the `sub-class of` property ([P279](https://www.wikidata.org/wiki/Property:P279)) property when the item refers to a category: Cat ([Q146](https://www.wikidata.org/wiki/Q146)) is a sub-class of mammal ([Q7377](https://www.wikidata.org/wiki/Q7377))

When the item is directly associated to several classes, we hypothesize that we can derive automatically a more concise description of the item using the knowledge formalized in wikidata.
This more concise description can result from (a possible mix of) these two situations:
- **summarization principle 1:** one of the direct classes subsumes (i.e. is more general than) another of the direct subclasses and we would like to hide the most general class as it does not provide additional information 
- **summarization principle 2:** two of the direct classes are not related and we would like the retrieve their lowest common ancestor(s) (which may not be unique).

The original motivation for this project was provided by Adriana Concha-Sepulveda.
She noticed that apple ([Q89](https://www.wikidata.org/wiki/Q89), so here a category) is a subclass of:
- fruit ([Q3314483](https://www.wikidata.org/wiki/Q3314483))
- pome ([Q41274](https://www.wikidata.org/wiki/Q41274))
- fruit of Maloideae ([Q145150](https://www.wikidata.org/wiki/Q145150))

This is an interesting example that combines both summarization principles, and we would like to automatically propose "fruit" as the best summary as:
- "pome" and "fruit of Maloideae" are not related by (direct or indirect) subsumption relations (although the description of pome states "type of fruit produced by plants in the subtribe Malinae"; **TODO:** investigates whether there is a relation between Malinae and Maloideae). This leads us to retrieve their lowest common ancestor(s)
- "fruit" subsumes the other two, and is actually their lowest common ancestor

# Todo

- [ ] generate a visual representation of the hierarchy of classes for a wikidata item(s)
    - [ ] take the item(s) as command-line parameters
    - [ ] use the labels
    - [x] add hyperlink to the item's and classes' wikidata URL
    - [ ] emphasize the classes directly associated to the item even if one subsumes the other
    - [ ] distinguish `instance of` and `sub-class of` for showing how the item of interest is associated with its direct class(es)
    - [ ] generalize to a set of wikidata items
- [ ] summarization algorithm
- [ ] analysis of wikidata
    - [ ] distribution of the number of direct annotations per item
        - [ ] distinguish `instance of` and `sub-class of` annotations
    - [ ] distribution of the number of ancestors
    - [ ] distribution of the summarization (number of direct annotations - number of summarized annotations) (NB: the LCA is not necessarily unique)
- Bibliography
    - [ ] Experimental data for computing semantic similarity between concepts using multiple inheritances in Wikipedia category graph. [https://www.ncbi.nlm.nih.gov/pubmed/32258267](https://www.ncbi.nlm.nih.gov/pubmed/32258267)


